xfe (1.46.2-1) unstable; urgency=medium

  * Update of many patches.
  * Update debian/copyright file.

  * debian/control:
    - Bump to Standards Version 4.7.0 (no changes).
    - Add build dependency libpolkit-gobject-1-dev.

 -- Joachim Wiedorn <joodebian@joonet.de>  Tue, 10 Dec 2024 23:47:48 +0100

xfe (1.46.1-1) unstable; urgency=medium

  * Update of some patches.
  * Remove patch 14 (german translation) and 16 (default app names).

  * Remove optional build dependency to policykit-1, let only pkexec.
  * Add two files to debian/clean (org.xfe.root.policy*). Closes: #1045407.
  * Update debian/copyright file.
  * Install new svg icons into icons/hicolor/scalable.

 -- Joachim Wiedorn <joodebian@joonet.de>  Sun, 10 Mar 2024 01:09:16 +0100

xfe (1.45-2) unstable; urgency=medium

  * Add pkexec | policykit-1 as (build) dependency and install file
    org.xfe.root.policy. Thanks to Roland Baudin. Closes: #1028556
  * Move build dependeny to libfreetype-dev.
  * Update translation patches (for de.po, POTFILES.in).

 -- Joachim Wiedorn <joodebian@joonet.de>  Sun, 26 Feb 2023 16:13:13 +0100

xfe (1.45-1) unstable; urgency=medium

  * New upstream release.
  * Update / remove / resort patches.
  * Update NEWS file.

 -- Joachim Wiedorn <joodebian@joonet.de>  Tue, 03 Jan 2023 18:03:48 +0100

xfe (1.44-2) unstable; urgency=medium

  * Add patches:
    - 18_xfwrite-fix-new-file   (fix problem with new files)
    - 19_update-m4-macros.patch
  * Add NEWS file (new HiDPI mode, old/new icon themes)
  * Bump to Standards Version 4.6.2 (no changes).

 -- Joachim Wiedorn <joodebian@joonet.de>  Fri, 30 Dec 2022 19:29:29 +0100

xfe (1.44-1) unstable; urgency=medium

  * New upstream release.
  * Update patch:
    - 06_no-mount-warning
  * Add some patches:
    - 12_other-suffixes-in-xferc
    - 13_video-suffixes-in-xferc
    - 14_update-german-translation
    - 16_no-translation-file
    - 17_change-default-apps

  * Bump to Standards Version 4.6.1 (no changes).
  * Move to debhelper-compat 13.
  * Add xfe suggestion xournalpp and avidemux.
  * Update watch file: now search for tar.xz files.

 -- Joachim Wiedorn <joodebian@joonet.de>  Tue, 22 Nov 2022 22:22:24 +0100

xfe (1.43.2-3) unstable; urgency=medium

  * Bump to Standards Version 4.5.1 (no changes).
  * Move to debhelper-compat 12.
  * Move build dependency to libxcb-util-dev.
  * Remove dependency xpdf and diffuse.
  * Install all icons into /usr/share/icons/hicolor.

 -- Joachim Wiedorn <joodebian@joonet.de>  Thu, 07 Jan 2021 17:40:32 +0100

xfe (1.43.2-2) unstable; urgency=medium

  * Make a source-only upload to Debian.
  * Bump to Standards Version 4.5.0 (no changes).

 -- Joachim Wiedorn <joodebian@joonet.de>  Mon, 02 Mar 2020 16:20:35 +0100

xfe (1.43.2-1) unstable; urgency=medium

  * New upstream release.
  * Remove obsolete debian/source/format file.
  * debian/control:
    - Move to debhelper-compat 11.
    - Bump to Standards Version 4.4.1 (no changes).
    - Remove obsolete build dependency to dh-autoreconf.
    - Add build dependency to libx11-xcb-dev. Closes: #951998
  * Update all patches.
  * Add patch fixing typo in german translation.

 -- Joachim Wiedorn <joodebian@joonet.de>  Sun, 23 Feb 2020 23:21:00 +0100

xfe (1.43.1-1) unstable; urgency=medium

  * New upstream release:
    - Solved: using the wrong pkg-config. Closes: #914586
    - Remove obsolete patches (09,21,22,23,25,27).
    - Update some patches.
    - Add new patch 13 for webm support.

  * debian/control:
    - Bump to Standards Version 4.2.1 (no changes).
    - Add build dependency to libxcb-util0-dev.

 -- Joachim Wiedorn <joodebian@joonet.de>  Mon, 26 Nov 2018 17:09:02 +0100

xfe (1.42.1-1) unstable; urgency=medium

  * New (unofficial) upstream release.
  * Update some patches. Remove obsolete patch (24). Add new patches:
    - Patch for pkgquery for multiarch (thanks to Michael Lange).
      Closes: #879758
    - Patch replacing unrar with free unar (thanks to Baptiste Jammet).
      Closes: #862968
    - Patch searching freetype2 with pkg-config. Closes: #892445
  * debian/control:
    - Bump to Standards Version: 4.1.3 (no changes).
    - Move to debhelper 10 and compat level 10.
    - Update Vcs links (salsa instead of alioth).

 -- Joachim Wiedorn <joodebian@joonet.de>  Fri, 16 Mar 2018 23:55:00 +0100

xfe (1.42-1) unstable; urgency=medium

  * New upstream release.
  * Update some patches.
  * Remove obsolete patches (25,26,27,28).
  * debian/control:
    - Bump to Standards-Version 3.9.8 (no changes).
    - Update VCS links, use secure https.
  * Update debian/copyright file, remove part about cvt (obsolete).
  * Update debian/watch file.

 -- Joachim Wiedorn <joodebian@joonet.de>  Fri, 29 Jul 2016 23:08:43 +0200

xfe (1.41-3) unstable; urgency=medium

  * Fix: optimize support for hurd and kfreebsd architecture (FilePanel).

 -- Joachim Wiedorn <joodebian@joonet.de>  Fri, 11 Dec 2015 09:10:54 +0100

xfe (1.41-2) unstable; urgency=medium

  * Fix: enable support for hurd and kfreebsd architecture
      (xfeutils, FilePanel).
  * Fix: check for startup notification only if enabled.
  * Fix: let us store in xfwrite to new file/filename.

 -- Joachim Wiedorn <joodebian@joonet.de>  Mon, 07 Dec 2015 20:46:16 +0100

xfe (1.41-1) unstable; urgency=medium

  * New upstream release.
  * Fix: Add build dependency to libfreetype6-dev. Closes: #806579
  * Fix: Check existing directories in rules file. Closes: #806663
  * Update some patches. Add update of German translation.
  * Remove xfe.menu file in preference of desktop files. See CTTE #741573

 -- Joachim Wiedorn <joodebian@joonet.de>  Fri, 04 Dec 2015 22:25:00 +0100

xfe (1.40-4) unstable; urgency=medium

  * Rewrite the bugfix for kfreebsd and hurd:
    let only the cmd variable always exist.

 -- Joachim Wiedorn <joodebian@joonet.de>  Sun, 19 Jul 2015 12:21:47 +0200

xfe (1.40-3) unstable; urgency=medium

  * Fix for kfreebsd and hurd: let pkg_format variable always exist.

 -- Joachim Wiedorn <joodebian@joonet.de>  Sat, 18 Jul 2015 16:15:18 +0200

xfe (1.40-2) unstable; urgency=medium

  * Fix for kfreebsd and hurd: let cmd variable always exist.

 -- Joachim Wiedorn <joodebian@joonet.de>  Sat, 18 Jul 2015 01:27:27 +0200

xfe (1.40-1) unstable; urgency=medium

  * New upstream release.
  * Bump to Standards Version 3.9.6 (no changes).
  * Add package suggestion to meld/diffuse/fldiff.

 -- Joachim Wiedorn <joodebian@joonet.de>  Sun, 12 Jul 2015 23:39:39 +0200

xfe (1.37-4) unstable; urgency=medium

  * Fix: Compile xvt/ttyinit.c always with utmp.h header.
  * Deprecated variable in configure.ac: move to AC_PROG_MKDIR_P.
  * Add subdir-objects as automake option in src/Makefile.am.
  * Fix debian/watch file to find upstream packages.

 -- Joachim Wiedorn <ad_debian@joonet.de>  Sat, 04 Oct 2014 10:16:00 +0200

xfe (1.37-3) unstable; urgency=medium

  * Fix (from xvt package): Portable BSD ctty baudrate.
  * Fix (from xvt package): Full utmp and wtmp (needed for kfreebsd).

 -- Joachim Wiedorn <ad_debian@joonet.de>  Mon, 29 Sep 2014 23:45:25 +0200

xfe (1.37-2) unstable; urgency=medium

  * Fix: Umask failure as root (CVE-2014-2079). Closes: #739536
  * Fix: Typo in German translation of archive suffixes.
  * Fix: use dh-autoreconf for better support for AArch64 architecture
         as a more general solution (updating aclocal.m4 and configure
         file).  Closes: #728006
  * Fix: Add Exec options to .desktop files (lintian warnings).

 -- Joachim Wiedorn <ad_debian@joonet.de>  Mon, 29 Sep 2014 09:42:22 +0200

xfe (1.37-1) unstable; urgency=low

  * New upstream release:
    - Add support for AArch64 architecture. Closes: #728006
    - Allow configuration of displayed dates. Closes: #642065
  * debian/control:
    - Bump to Standards-Version 3.9.5 (no changes).
  * Update debian/rules.
  * Update all patches.
  * Add patch: check new path of freetype header files (configure).

 -- Joachim Wiedorn <ad_debian@joonet.de>  Thu, 26 Dec 2013 21:05:05 +0100

xfe (1.34-2) unstable; urgency=low

  * Change section of -i18n package to localization.
  * Fix: set default tabcols value to 8.
  * Fix: don't use two refresh functions on kfreebsd. Closes: #708739

 -- Joachim Wiedorn <ad_debian@joonet.de>  Sun, 02 Jun 2013 20:38:08 +0200

xfe (1.34-1) unstable; urgency=low

  * New upstream release.
  * debian/control:
    - Move to debhelper (>= 9).
    - Remove obsolete DM-Upload-Allowed flag.
    - Bump to Standards Version 3.9.4 (no changes).
  * Move to compat level 9.
  * Reduce menu icons to 32x32 (lintian).
  * Remove obsolete patches, update some patches.
  * Add two patches:
    - remove backup file (autosave).
    - update german translation.

 -- Joachim Wiedorn <ad_debian@joonet.de>  Wed, 01 May 2013 20:59:15 +0200

xfe (1.32.5-2) unstable; urgency=low

  * Fix: Remove manually symlinks in xfe-i18n and xfe-themes
      to doc directory of xfe in preinst scripts. Closes: #700068

 -- Joachim Wiedorn <ad_debian@joonet.de>  Sat, 09 Feb 2013 16:33:06 +0100

xfe (1.32.5-1) unstable; urgency=low

  * New upstream release.
    - Fix bug with drag and drop.
    - Fix duplicated shortcuts in bookmarks menu.
    - Fix crash on xfpack (XFilePackage).
    - Add support for xz and tar.xz archive format.

  * Optimize description in debian/control.  (Closes: #669040)
  * Remove all (old) patches. Add all the updated patches.
  * Update of debian/copyright file.
  * Fix small typo in translation files.
  * Update german translation (de.po).

 -- Joachim Wiedorn <ad_debian@joonet.de>  Wed, 09 May 2012 19:57:57 +0200

xfe (1.32.4-4) unstable; urgency=low

  * Move to build dependency libpng-dev.  (Closes: #662558)
  * Bump to Standards Version 3.9.3 (no changes).

 -- Joachim Wiedorn <ad_debian@joonet.de>  Tue, 06 Mar 2012 16:32:06 +0100

xfe (1.32.4-3) unstable; urgency=low

  * debian/control:
    - Update suggested packages: remove playmidi, change to xine-ui.
  * debian/patches:
    - Update some patches.
    - Optimize no-mount-warning patch. By default do not warn while mounting.
    - Add not-asking-for-quit patch. By default do not ask for quit. (Closes:
      #642078)

 -- Joachim Wiedorn <ad_debian@joonet.de>  Tue, 18 Oct 2011 14:17:15 +0200

xfe (1.32.4-2) unstable; urgency=low

  * Optimize support for font files especially for fontforge.
  * Optimize mimetypes in desktop files. (Closes: #636091)
  * Fix ld problem with --as-needed. (Closes: #632151, LP: #803216)

 -- Joachim Wiedorn <ad_debian@joonet.de>  Tue, 06 Sep 2011 00:15:20 +0200

xfe (1.32.4-1) unstable; urgency=low

  * New upstream release:
    - Fix compiling errors when use binutils-gold. (Closes: #556704)
    - Fix opening root window as a regular user. (Closes: #628030)
    - Fix crashes during drag and drop operations. (Closes: #628069)
  * debian/patches:
    - Update some patches.
    - Add patch for support of xournal (pdf editor) in xferc.
  * Update debian/copyright.
  * debian/control:
    - Bump to Standards-Version 3.9.2 (no changes).
    - Move to debhelper (>= 8)
    - Update Vcs Git and Browser URLs.
    - Optimize short package descriptions.
  * Move to compatibility level 8.

 -- Joachim Wiedorn <ad_debian@joonet.de>  Wed, 15 Jun 2011 20:19:08 +0200

xfe (1.32.3-1) unstable; urgency=low

  * New upstream release:
    - Fix of start window position. (Closes: #602602)
  * Remove obsolete patches. Update all the other patches.
  * debian/control:
    - Recommends xarchiver, audacious, playmidi for xfe.
    - Suggests xpdf and xine for xfe.
    - Remove dependency to libcups2(-dev) (already in libfox-1.6).
    - Remove dependency to libstartup-notification0(-dev) (embedded in xfe).
    - Remove dependency to cdbs. Use debhelper (>= 7.0.50~).
    - Add Vcs Git and Browser URLs to new git repository.
    - Optimize dependencies between xfe binary packages.
  * Upgrade of debian/rules for use with newer debhelper, without cdbs.
  * Move all icon themes into xfe-themes package.
  * Small update of debian/copyright to newer standard.
  * Move to libreoffice support in xferc.
  * Change imageviewer preference for 'open' and 'view'.

 -- Joachim Wiedorn <ad_debian@joonet.de>  Mon, 28 Mar 2011 22:50:23 +0200

xfe (1.32.1-6) unstable; urgency=low

  * debian/control: resolve circular dependency. (Closes: #598843)

 -- Joachim Wiedorn <ad_debian@joonet.de>  Sun, 03 Oct 2010 22:00:44 +0200

xfe (1.32.1-5) unstable; urgency=low

  * debian/patches:
    - Set gnomeblue-theme as default as defined upstream (patch removed).
    - Update of german translation (thanks to Jens Körner).
  * debian/control:
    - Set package xfe-themes as dependency of package xfe. (Closes: #570205)
    - Remove unsupported Bugs field. (Closes: #591217)
    - Update xfe package dependency to audacious (>> 2.3-1).
    - Use 'Breaks:' field instead of 'Conflicts:' field.
    - Bump to Standards version 3.9.1.
    - Remove Uploader (was no Co-Maintainer).
  * Use correct upstream licenses in debian/copyright and use new format.
  * Remove obsolete file debian/README.source.
  * Fix open error with program xfpack. (Closes: #593215)

 -- Joachim Wiedorn <ad_debian@joonet.de>  Sun, 19 Sep 2010 16:57:50 +0200

xfe (1.32.1-4) unstable; urgency=low

  * debian/patches:
    - Change config inside xvt for hurd systems.

 -- Joachim Wiedorn <ad_debian@joonet.de>  Tue, 02 Feb 2010 15:01:35 +0100

xfe (1.32.1-3) unstable; urgency=low

  * debian/patches:
    - Patch for non inline functions. (Closes: #560549)
    - Add config inside xvt for kfreebsd and hurd systems.
    - Remove encoding line in desktop files.
    - Change to new header standard for patch files.
  * debian/control:
    - Update Uploader field.
    - Add recommended xterm for xfe.

 -- Joachim Wiedorn <ad_debian@joonet.de>  Fri, 29 Jan 2010 20:33:22 +0100

xfe (1.32.1-2) unstable; urgency=low

  * debian/control:
    - Add second uploader name.
    - Add recommends xfe-i18n and xfe-themes to xfe package.
    - Add version number to dependency audacious (>= 2.0.1).
  * Remove ccache for building packages. (Closes: #558338)
  * Bug about hungry CPU was fixed upstream. (Closes: #362225)
  * Bug about use of single trash was fixed upstream. (Closes: #368768)

 -- Joachim Wiedorn <ad_debian@joonet.de>  Sat, 28 Nov 2009 09:32:12 +0100

xfe (1.32.1-1) unstable; urgency=low

  * New upstream release. (Closes: #529693, #529855, #311019, #530431, #500670)
      (Closes: #362224, #505855, #355617, #368767, #469572, #475940, #471578)
  * debian/control:
    - Update to debhelper (>= 7).
    - Change to Standards-Version 3.8.3.
    - Add dependency libcups2 to package xfe. (Closes: #520439)
    - Add build dependency libcups2-dev and intltool.
  * Change to Debian source format 3.0 (quilt).
  * Split into three packages: xfe, xfe-i18n and xfe-themes
      and set links to the doc directory of xfe package.
  * Using application names: xfimage, xfview, xfwrite, xfpack
      and set preferences to right application names. (Closes: #445721)
  * Add some new suffixes to xferc (mk, asc, mp4).
  * Add minor update to german translation

 -- Joachim Wiedorn <ad_debian@joonet.de>  Mon, 16 Nov 2009 09:32:10 +0100

xfe (1.19.1-2) unstable; urgency=low

  * New upstream release.
  * Using application names: xfimage, xfview, xfwrite, xfpack.
  * Updated xferc.in with ooxml formats and odb format.

 -- Joachim Wiedorn <ad_debian@joonet.de>  Thu, 23 Apr 2009 18:19:10 +0100

xfe (1.04-2.1) unstable; urgency=low

  [ Sandro Tosi ]
  * Non-maintainer upload.
  * debian/patches/10_bts-442769.dpatch
    - added to avoid removal of a makefile needed for clean target (Closes:
      #442769)
  * debian/rules
    - added clean target to remove a makefile only at the end of target itself

 -- Pierre Habouzit <madcoder@debian.org>  Sat, 05 Apr 2008 11:38:35 +0000

xfe (1.04-2) unstable; urgency=low

  * debian/control:
    - Bump Standards-Version: 3.7.3.
    - Added Homepage: header.

 -- Jose Carlos Medeiros <debian@psabs.com.br>  Sat, 22 Mar 2008 20:06:45 -0300

xfe (1.04-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Apply again Andreas Henriksson's patch for conditional ucf call to postrm.
    Closes: #413254

 -- David Ammouial <da-deb@weeno.net>  Sat, 29 Sep 2007 00:15:15 +0200

xfe (1.04-1) unstable; urgency=low

  * New upstream release

 -- Jose Carlos Medeiros <debian@psabs.com.br>  Fri, 21 Sep 2007 17:58:59 -0300

xfe (1.00-1) unstable; urgency=low

  * New upstream release
  * Updated sections and titles menu, debian/menu. (Closes: #432109)
  * Added 01_xferc_update.dpatch to update xferc with correct file name of
    applications xfe, xfp, xfw, xfi and xfv.
  * xfilequery was changed to xfilepackage, created links to keep
    compatibility.

 -- Jose Carlos Medeiros <debian@psabs.com.br>  Thu, 19 Jul 2007 22:04:30 -0300

xfe (0.99-2) unstable; urgency=low

  * Since 0.98 version, these bugs were solved. 
    (Closes: #372305, #362021, #389811)
  * Solved error during upgrade in postinst script. (Closes: #421721)

 -- Jose Carlos Medeiros <debian@psabs.com.br>  Fri, 25 May 2007 14:58:52 -0300

xfe (0.99-1) unstable; urgency=low

  * New upstream release
  * Removed no more used 04_german_translation.dpatch
  * Created dh_link to conf file. As upstream change location from /etc to
    /usr/share.

 -- Jose Carlos Medeiros <debian@psabs.com.br>  Thu, 17 May 2007 23:38:47 -0300

xfe (0.98-1) unstable; urgency=low

  * New upstream release. (Closes: #365360, #371145, #391540)
  * Removed no more used patches 01_icon_path.dpatch, 02_fox_suffix.dpatch,
    08_brazilian_translation.dpatch
  * Updated to use libfox-1.6-dev.
  * Added Homepage in control file.
  * Removed debian/NEWS file.
  * Added xfw in menu, added icon and renamed to xfilewriter.
  * Updated to DH_COMPAT 5, debhelper (>= 5.0.0)

 -- Jose Carlos Medeiros <debian@psabs.com.br>  Wed, 14 Feb 2007 10:22:43 -0200

xfe (0.88-3.1) unstable; urgency=low

  * Non-maintainer upload.
  * Applyed patch for purge problems on `debian/postrm', thanks to Florian 
    Schlichting. (Closes: #384358)

 -- Jose Luis Rivas <ghostbar38@gmail.com>  Sat, 16 Sep 2006 23:45:10 -0400

xfe (0.88-3) unstable; urgency=low

  * Added 09_fix_wrong_config_path patch to change path where config files are
    put to complain with Policy 10.7.2 and 10.7.3. Thanks to Mike O'Connor
    <stew@vireo.org>. (Closes: #375542)
    - Added debian/postinst file.
    - Added debian/postrm file.
    - Changed debian/dirs file.
    - Changed debian/rules file.
    - Added ucf as Dependence.
  * Bump Standards-Version: 3.7.2.

 -- Jose Carlos Medeiros <debian@psabs.com.br>  Thu, 17 Aug 2006 15:37:03 -0300

xfe (0.88-2) unstable; urgency=low

  * Added 08_brazilian_translation patch with updated po Brazilian translation.
  * Removed 06_ptr_conversion patch. Upstreamer patched in 0.88 version.
  * Updated 07_no_mount_warning patch to do not show mount error. 
    (closes: #311018)

 -- Jose Carlos Medeiros <debian@psabs.com.br>  Mon, 13 Mar 2006 12:24:12 -0300

xfe (0.88-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * debian/rules: Let dpatch.mk handle unpatching.  Closes: #352347.

 -- Matej Vela <vela@debian.org>  Sat, 11 Mar 2006 10:46:41 +0100

xfe (0.88-1) unstable; urgency=low

  * New upstream release.
    - Improved Description in control file.
    - Disabled 03_file_extensions patch, upstreamer solved this problem.
    - Disabled 04_german_translation patch, upstreamer solve this problem.
    - Added short GPL text license in debian/copyright.

 -- Jose Carlos Medeiros <debian@psabs.com.br>  Thu,  9 Feb 2006 15:15:57 -0200

xfe (0.84-4) unstable; urgency=low

  * New Mantainer. (Closes: #334619)
  * Updated debian/watch file.

 -- Jose Carlos Medeiros <debian@psabs.com.br>  Fri, 21 Oct 2005 14:15:24 -0200

xfe (0.84-3) unstable; urgency=low

  * Orphaning the package (see #334619)

 -- Bastian Kleineidam <calvin@debian.org>  Wed, 19 Oct 2005 02:29:33 +0200

xfe (0.84-2) unstable; urgency=low

  * Use new watch file format and qa.debian SF redirector
  * Ignore mount points with missing (read) permission (Closes: #301125)

 -- Bastian Kleineidam <calvin@debian.org>  Thu, 21 Jul 2005 09:10:41 +0200

xfe (0.84-1) unstable; urgency=low

  * New upstream release.
    - Fixes recursive permission setting on a single folder
      (Closes: #228120)
    - Fixes sorting by file type
      (Closes: #309997)
    - Fixes file selection problem
      (Closes: #300982)
    - Fixes file attribute extra character bug
      (Closes: #301757)
  * Apply german po updates, thanks to Jens Seidel (Closes: #313877)
  * Depend on FOX 1.4, and since it is compiled with GCC 4, build-depend
    on that compiler version.
  * Use Standards version 3.6.2.1
  * Scale and convert program icons to xpm, and added them to the
    menu file.

 -- Bastian Kleineidam <calvin@debian.org>  Thu, 14 Jul 2005 23:01:39 +0200

xfe (0.72-6) unstable; urgency=high

  * build depend on fox1.2 >= 1.2.13 to be sure having a working .shlibs
    file
  * Fix the identity test to compare inodes rather than only names. This
    resulted in false negatives on case-independent file systems (eg.
    vfat). (Closes: #303466)

 -- Bastian Kleineidam <calvin@debian.org>  Sun, 10 Apr 2005 20:53:26 +0200

xfe (0.72-5) unstable; urgency=medium

  * Added shlibs.local file to add libFOX-1.2 to the dependencies
    (Closes: #299196) - xfe does not declare dependency on libfox 1.2

 -- Bastian Kleineidam <calvin@debian.org>  Mon, 14 Mar 2005 11:52:51 +0100

xfe (0.72-4) unstable; urgency=low

  * Upload to unstable.

 -- Bastian Kleineidam <calvin@debian.org>  Wed,  9 Mar 2005 10:10:32 +0100

xfe (0.72-3) experimental; urgency=low

  * New patch 06_ptr_conversion from Andreas Jochens:
    fix pointer conversion on amd64 platforms using gcc-4.0

 -- Bastian Kleineidam <calvin@debian.org>  Thu,  3 Mar 2005 15:31:02 +0100

xfe (0.72-2) experimental; urgency=low

  * Disable thumbnail display per default (Closes: #294390)

 -- Bastian Kleineidam <calvin@debian.org>  Thu, 10 Feb 2005 16:38:57 +0100

xfe (0.72-1) experimental; urgency=low

  * New upstream release.
    - 02_consistent_button_order removed, applied upstream
  * fix debian/watch url
  * compile with FOX 1.2 (thus upload to experimental)
  * use cdbs to build the package
  * 04_update_de_po: updated german translation (not complete, will
    do that next release).

 -- Bastian Kleineidam <calvin@debian.org>  Tue,  3 Aug 2004 09:22:06 +0200

xfe (0.66-5) unstable; urgency=low

  * icon path patch was incomplete, also fixed the missing parts
    (Closes: #261668)
  * added .wri extension to default desktop
  * replace old xlibs-dev build-dependency with new libx11-dev, libxext-
    dev

 -- Bastian Kleineidam <calvin@debian.org>  Tue, 27 Jul 2004 16:37:32 +0200

xfe (0.66-4) unstable; urgency=low

  * fix menu commands to use the renamed binaries, thanks Itamar Ravid
    (Closes: #256767)

 -- Bastian Kleineidam <calvin@debian.org>  Tue, 29 Jun 2004 12:51:06 +0200

xfe (0.66-3) unstable; urgency=low

  * make accept/cancel button order consistent with the one found in
    standard FOX toolkit dialogs (Closes: #246348)

 -- Bastian Kleineidam <calvin@debian.org>  Thu, 29 Apr 2004 12:20:04 +0200

xfe (0.66-2) unstable; urgency=low

  * fix icon path from /usr/lib/foxicons to /usr/share/foxicons in
    the default preferences. Thanks to Greek0 for the patch.
    (Closes: #242373)
  * quote all strings in debian/menu

 -- Bastian Kleineidam <calvin@debian.org>  Wed,  7 Apr 2004 19:43:23 +0200

xfe (0.66-1) unstable; urgency=low

  * New upstream release, removed 01_no_path_quote applied upstream

 -- Bastian Kleineidam <calvin@debian.org>  Wed, 18 Feb 2004 01:31:17 +0100

xfe (0.60-4) unstable; urgency=low

  * Dang, renamed _again_ (previous rename was lost somehow) xfq and xfv
    to xfilequery and xfileview to prevent clash with other packages
    (Closes: #224444)

 -- Bastian Kleineidam <calvin@debian.org>  Fri, 19 Dec 2003 10:59:02 +0100

xfe (0.60-3) unstable; urgency=low

  * new patch:
    - 01_no_path_quote:
      do not quote pathnames on NewDir and NewFile (Closes: #221733)

 -- Bastian Kleineidam <calvin@debian.org>  Wed, 19 Nov 2003 21:56:07 +0100

xfe (0.60-2) unstable; urgency=low

  * Add Build-depends libpng12-dev, xlibs-dev (Closes: #221475)

 -- Bastian Kleineidam <calvin@debian.org>  Tue, 18 Nov 2003 18:31:30 +0100

xfe (0.60-1) unstable; urgency=low

  * New upstream release.
    - umount device busy error resolved (Closes: #215995)

 -- Bastian Kleineidam <calvin@debian.org>  Tue, 18 Nov 2003 14:36:46 +0100

xfe (0.54.2-3) unstable; urgency=low

  * added debian/watch file
  * Standards version 3.6.1

 -- Bastian Kleineidam <calvin@debian.org>  Tue,  7 Oct 2003 21:18:23 +0200

xfe (0.54.2-2) unstable; urgency=low

  * rename xfq and xfv to xfilequery and xfileview to prevent clash with
    other packages (Closes: #204846)
  * add AM_ to AM_CPPFLAGS and AM_LDFLAGS
  * include iostream, not iostream.h

 -- Bastian Kleineidam <calvin@debian.org>  Mon, 11 Aug 2003 16:58:01 +0200

xfe (0.54.2-1) unstable; urgency=low

  * Initial packaging (Closes: #190758)

 -- Bastian Kleineidam <calvin@debian.org>  Sun, 26 Jan 2003 20:03:17 +0100
