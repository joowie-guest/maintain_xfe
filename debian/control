Source: xfe
Section: x11
Priority: optional
Maintainer: Joachim Wiedorn <joodebian@joonet.de>
Build-Depends:
    debhelper-compat (= 13),
    intltool,
    libfox-1.6-dev (>= 1.6.45),
    libfreetype-dev,
    libpng-dev,
    libpolkit-gobject-1-dev,
    libxft-dev,
    libx11-dev,
    libx11-xcb-dev,
    libxcb-util-dev,
    pkexec
Standards-Version: 4.7.0
Homepage: http://roland65.free.fr/xfe/
Vcs-Git: https://salsa.debian.org/joowie-guest/maintain_xfe.git
Vcs-Browser: https://salsa.debian.org/joowie-guest/maintain_xfe

Package: xfe
Architecture: any
Depends: xfe-themes (>= ${source:Version}),
 ${shlibs:Depends}, ${misc:Depends}
Recommends: xfe-i18n, xterm, xarchiver,
 pkexec, unar, audacious, atril | qpdfview
Suggests: rpm, parole | xine-ui, gimp,
 meld | diffuse, xournalpp, avidemux
Description: lightweight file manager for X11
 Xfe is based on the popular but discontinued X Win Commander. It is
 desktop independent and is written using the C++ Fox Toolkit. Its 
 appearance is similar to the Windows file-manager Total Commander
 or Windows Explorer. It is very fast and simple.
 .
 The main features are: file associations, mount/umount devices,
 directory tree for quick cd, change file attributes, auto save 
 registry, compressed archives view/creation/extraction, compatibility
 with GNOME/KDE/Xfce, and much more. 
 .
 Containing a simple text editor (Xfwrite), image viewer (Xfimage) and
 package manager (Xfpack).

Package: xfe-i18n
Architecture: all
Section: localization
Depends: xfe (>= ${source:Version}), ${misc:Depends}
Description: lightweight file manager for X11 (i18n support)
 Xfe is based on the popular but discontinued X Win Commander. It is
 desktop independent and is written using the C++ Fox Toolkit. Its 
 appearance is similar to the Windows file-manager Total Commander
 or Windows Explorer. It is very fast and simple.
 .
 This package provides support many locales for non-English interfaces.

Package: xfe-themes
Architecture: all
Depends: ${misc:Depends}
Breaks: xfe (<= 1.32.2)
Replaces: xfe (<= 1.32.2)
Description: lightweight file manager for X11 (themes)
 Xfe is based on the popular but discontinued X Win Commander. It is
 desktop independent and is written using the C++ Fox Toolkit. Its 
 appearance is similar to the Windows file-manager Total Commander
 or Windows Explorer. It is very fast and simple.
 .
 This package contains all icon themes for xfe and its utils.
